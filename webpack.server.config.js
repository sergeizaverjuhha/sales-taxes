const path = require('path');
const webpack = require('webpack');

module.exports = {
  mode: 'none',
  entry: {
    // Compile ts to js
    server: './src/server.ts'
  },
  target: 'node',
  resolve: { extensions: ['.ts', '.js'],
 },
  optimization: {
    minimize: false
  },
  output: {
    path: path.join(__dirname, 'server'),
    filename: '[name].js'
  },
  externals: [
    'pg-native'
  ],
  module: {
    rules: [
      { test: /\.ts$/, loader: 'ts-loader' },
      {
        // Mark files inside `@angular/core` as using SystemJS style dynamic imports.
        // Removing this will cause deprecation warnings to appear.
        test: /(\\|\/)@angular(\\|\/)core(\\|\/).+\.js$/,
        parser: { system: true },
      },
    ]
  },
  plugins: [
    // Fixes WARNINGS Critical dependency
    new webpack.ContextReplacementPlugin(
      /(.+)?angular(\\|\/)core(.+)?/,
      path.join(__dirname, 'src'),
      {} // a map of the routes
    ),
    new webpack.ContextReplacementPlugin(
      /(.+)?express(\\|\/)(.+)?/,
      path.join(__dirname, 'src'),
      {}
    )
  ]
};
