import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import * as items from './app/shared/shopping-cart.router';

const port = 3000;
const app = express();
app.use(cors());

app.use(bodyParser.json());

app.get('/items', items.getItems);
app.post('/items', items.createItem);
app.put('/items/:id', items.updateItem);
app.delete('/items/:id', items.deleteItem);

app.get('/api', (req, res, next) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
});

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}/`);
});
