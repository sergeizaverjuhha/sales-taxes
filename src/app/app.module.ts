import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpMockRequestInterceptor } from './tests/http-mock-request-interceptor';
import { environment } from 'src/environments/environment.mock';
import { ApiService } from './services/api.service';
import { CalculateRowPricePipe } from './pipes/calculate-row-price.pipe';
import { CalculateTotalPricePipe } from './pipes/calculate-total-price.pipe';

export const isMock = environment.mock;

@NgModule({
  declarations: [
    AppComponent,
    CalculateRowPricePipe,
    CalculateTotalPricePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatInputModule,
    MatSelectModule,
    MatTableModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    {
      provide: isMock ? HTTP_INTERCEPTORS : HttpClientModule,
      useClass: isMock ? HttpMockRequestInterceptor : ApiService,
      multi: isMock ? true : false
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
