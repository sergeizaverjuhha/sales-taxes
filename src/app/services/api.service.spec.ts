import { TestBed } from '@angular/core/testing';
import { ApiService } from './api.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ShoppingCart } from '../shared/shopping-cart.model';

describe('ApiService', () => {
  let service: ApiService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ApiService]
    });
    service = TestBed.inject(ApiService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should fetch items as an Observable', () => {

    const expectedItem: ShoppingCart[] =
      [{ id: 1, item: 'item', category: 'category', amount: 1, price: 0 }];

    service.getItems().subscribe(response => {
      expect(response.length).toBe(1);
    });

    const httpRequest = httpMock.expectOne('http://localhost:3000/items');
    expect(httpRequest.request.method).toBe('GET');
    httpRequest.flush(expectedItem);

  });
});
