import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { ShoppingCart } from 'src/app/shared/shopping-cart.model';
import { catchError } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders(
    { 'Content-Type': 'application/json' }
  )
};
const apiUrl = 'http://localhost:3000/items';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  private handleError<T>(result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getItems(): Observable<ShoppingCart[]> {
    return this.http.get<ShoppingCart[]>(apiUrl)
      .pipe(catchError(this.handleError([])));
  }

  addItem(item: ShoppingCart): Observable<ShoppingCart> {
    return this.http.post<ShoppingCart>(apiUrl, item, httpOptions)
      .pipe(catchError(this.handleError(item)));
  }

  updateItem(id: number, item: ShoppingCart): Observable<any> {
    const putUrl = `${apiUrl}/${id}`;
    return this.http.put(putUrl, {amount: item.amount}, httpOptions)
    .pipe(catchError(this.handleError<any>()));
  }

  deleteItem(id: number): Observable<ShoppingCart> {
    const deleteUrl = `${apiUrl}/${id}`;
    return this.http.delete<ShoppingCart>(deleteUrl, httpOptions)
    .pipe(catchError(this.handleError<ShoppingCart>()));
  }
}
