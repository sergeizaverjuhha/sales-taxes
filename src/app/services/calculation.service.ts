import { Injectable } from '@angular/core';
import { ShoppingCart } from '../shared/shopping-cart.model';

@Injectable({
  providedIn: 'root'
})
export class CalculationService {

  parseItemString(itemString: string): any {
    const stringToObject = { item: '', price: '', amount: '' };
    const indexOfAt =  itemString.indexOf(' at ');
    const indexOfTheFirstNumber = itemString.search(/\d/);
    if (itemString.includes(' at ')
      && (!isNaN(parseFloat(itemString.substr(indexOfAt + 4))))
    ) {
      stringToObject.item = itemString.substr(0, indexOfAt);
      stringToObject.price = itemString.substr(indexOfAt + 4);
    }
    if (indexOfTheFirstNumber > -1 && indexOfAt > indexOfTheFirstNumber) {
      stringToObject.amount = itemString.substr(indexOfTheFirstNumber, itemString.search(' '));
      stringToObject.item = itemString.substr(itemString.search(' '), indexOfAt);
    }
    return stringToObject;
  }

  calculateNetto(item: ShoppingCart): number {
    return +item.price;
  }

  calculateTax(item: ShoppingCart): number {
    const isImported = item.item.includes('imported');
    const isOtherCategory = item.category.includes('Other');
    const standardTax = 0.1;
    const importTax = 0.05;
    let tax = 0;
    if (isImported && !isOtherCategory) {
      tax = importTax * item.price;
    } else if (isImported && isOtherCategory) {
      tax = (importTax + standardTax) * item.price;
    } else if (!isImported && isOtherCategory) {
      tax = standardTax * item.price;
    }
    return Math.ceil(tax * 20) / 20.0;
  }

  calculatePrice(item: ShoppingCart): number {
    return this.calculateNetto(item) + this.calculateTax(item);
  }
}
