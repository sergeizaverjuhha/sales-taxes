import { TestBed } from '@angular/core/testing';
import { CalculationService } from './calculation.service';
import { HttpClientModule } from '@angular/common/http';

describe('CalculationService', () => {
  let service: CalculationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [CalculationService]
    });
    service = TestBed.inject(CalculationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should parse the item string', () => {
    const result = service.parseItemString('1 imported box of chocolates at');
    expect(result.price).toEqual('');
  });

  it('should extract the price of an item', () => {
    const result = service.parseItemString('1 book at 12.49');
    expect(result.price).toEqual('12.49');
  });

  it('should be the imported rate of tax with exemptions', () => {
    const result = service.parseItemString('1 imported box of chocolates at 10.00');
    const item = { id: 1, item: result.item, category: 'Food', amount: result.amount, price: result.price };
    const priceToPay = service.calculatePrice(item);
    const calculatedPrice = +result.price + result.price * 0.05;
    expect(priceToPay).toEqual(calculatedPrice);
  });

  it('should be the standard rate of tax', () => {
    const result = service.parseItemString('1 music CD at 14.99');
    const item = { id: 1, item: result.item, category: 'Other', amount: result.amount, price: result.price };
    const priceToPay = service.calculatePrice(item);
    const calculatedPrice = +result.price + Math.ceil((+result.price * 0.1) * 20) / 20.0;
    expect(priceToPay).toEqual(calculatedPrice);
  });

  it('should be the imported standard rate of tax', () => {
    const result = service.parseItemString('1 box of imported chocolates at 11.25');
    const item = { id: 1, item: result.item, category: 'Food', amount: result.amount, price: result.price };
    const priceToPay = service.calculatePrice(item);
    const calculatedPrice = Math.ceil((+result.price + result.price * 0.05) * 20) / 20.0;
    expect(priceToPay).toEqual(calculatedPrice);
  });

  it('should be the netto subtotal price', () => {
    const parseItem1 = service.parseItemString('1 packet of headache pills at 9.75');
    const item1 = { id: 1, item: parseItem1.item, category: 'Medicaments', amount: parseItem1.amount, price: parseItem1.price };
    const parseItem2 = service.parseItemString('1 imported box of chocolates at 10.00');
    const item2 = { id: 2, item: parseItem2.item, category: 'Food', amount: parseItem2.amount, price: parseItem2.price };
    const netto = (+parseItem1.price) + (+parseItem2.price);
    const result = service.calculateNetto(item1) + service.calculateNetto(item2);
    expect(result).toEqual(netto);
  });

  it('should be the sum of sales taxes', () => {
    const parseItem1 = service.parseItemString('1 box of imported chocolates at 11.25');
    const item1 = { id: 1, item: parseItem1.item, category: 'Food', amount: parseItem1.amount, price: parseItem1.price };
    const parseItem2 = service.parseItemString('1 imported box of chocolates at 10.00');
    const item2 = { id: 2, item: parseItem2.item, category: 'Food', amount: parseItem2.amount, price: parseItem2.price };
    const taxes = Math.ceil((+item1.price * 0.05) * 20) / 20 + Math.ceil((+item2.price * 0.05) * 20) / 20;
    const result = service.calculateTax(item1) + service.calculateTax(item2);
    expect(result).toEqual(taxes);
  });

});
