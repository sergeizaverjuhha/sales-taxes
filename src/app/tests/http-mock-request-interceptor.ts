import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import * as items from './items.json';

const urls = [
  {
    url: 'http://localhost:3000/items',
    json: items
  }
];

@Injectable({
  providedIn: 'root'
})
export class HttpMockRequestInterceptor implements HttpInterceptor {

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    for (const element of urls) {
      if (request.url === element.url) {
        console.log('Loaded mock json');
        return of(new HttpResponse({ status: 200, body: ((element.json) as any).default }));
      }
    }
    return next.handle(request);
  }
}
