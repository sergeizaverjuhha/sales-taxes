import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { MatSelectModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { ApiService } from './services/api.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CalculationService } from './services/calculation.service';

let apiService: ApiService;
let calcService: CalculationService;

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatSelectModule,
        FormsModule,
        HttpClientModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [ApiService, CalculationService]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should create an instance of the api service', () => {
    apiService = TestBed.inject(ApiService);
    expect(apiService).toBeTruthy();
  });

  it('should create an instance of the calculation service', () => {
    calcService = new CalculationService();
    expect(calcService).toBeTruthy();
  });

});
