import { postgresdb } from '../config/db.config';

const selectQuery = 'SELECT * FROM shoppingcart ORDER BY id ASC';
const insertQuery = 'INSERT INTO shoppingcart(item, category, amount, price) VALUES ($1, $2, $3, $4)';
const updateQuery = 'UPDATE shoppingcart SET amount = $1 WHERE id = $2';
const deleteQuery = 'DELETE FROM shoppingcart WHERE id = $1';

export const getItems = (request: any, response: any) => {
  postgresdb.query(selectQuery, (error, results) => {
    console.log(results.rows);
    if (error) {
      throw error
    }
    response.status(200).json(results.rows);
  })
};

export const createItem = (request: any, response: any) => {
  const {item, category, amount, price } = request.body;
  postgresdb.query(insertQuery, [item, category, amount, price], (error, result) => {
    if (error) {
      throw error
    }
    response.status(201).json(`Item: ${item} added`);
  })
};

export const updateItem = (request: any, response: any) => {
  const id = parseInt(request.params.id)
  const { amount } = request.body
  postgresdb.query(updateQuery, [amount, id], (error, result) => {
      if (error) {
        throw error
      }
      response.status(200).json(`Item: ${id} modified`)
    }
  )
};

export const deleteItem = (request: any, response: any) => {
  const id = parseInt(request.params.id)
  postgresdb.query(deleteQuery, [id], (error, result) => {
    if (error) {
      throw error
    }
    response.status(200).json(`Item ${id} deleted`)
  })
};
