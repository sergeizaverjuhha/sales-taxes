export class ShoppingCart {
  id: number;
  item: string;
  category: string;
  amount: number;
  price: number;
}
