import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'calculateTotalPrice'
})
export class CalculateTotalPricePipe implements PipeTransform {
 /** Get the total price of all items. */
  transform(total: number[]): number {
    return total.reduce((acc, value) => acc + value, 0);
  }
}
