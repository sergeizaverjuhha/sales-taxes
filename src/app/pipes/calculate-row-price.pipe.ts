import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'calculateRowPrice'
})

export class CalculateRowPricePipe implements PipeTransform {
  /** Get the total price of one row. */
  transform(subtotal: number, ...args: number[]): number {
    return args.reduce((acc, value) => acc * value, 1);
  }
}
