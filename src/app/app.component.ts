import { Component, OnInit, LOCALE_ID } from '@angular/core';
import { ShoppingCart } from './shared/shopping-cart.model';
import { NgForm, FormControl, Validators } from '@angular/forms';
import { ApiService } from './services/api.service';
import { registerLocaleData } from '@angular/common';
import locales from '../../node_modules/@angular/common/locales/de';
import { CalculationService } from './services/calculation.service';
import { CalculateTotalPricePipe } from './pipes/calculate-total-price.pipe';

interface Categories {
  key: string;
  value: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'de'
    },
    CalculateTotalPricePipe
  ]
})

export class AppComponent implements OnInit {

  constructor(
    private api: ApiService,
    private calculationService: CalculationService,
    private calculateTotalPrice: CalculateTotalPricePipe
  ) {
    registerLocaleData(locales, 'de');
  }

  shoppingCart: ShoppingCart[];
  subtotal: number;
  subtotals: number[];
  netto: number;
  nettos: number[];
  taxes: number[];
  tax = 0;
  total = 0;

  // drop-down list options
  categories: Categories[] = [
    { key: 'books-1', value: 'Books' },
    { key: 'food-2', value: 'Food' },
    { key: 'medicaments-3', value: 'Medicaments' },
    { key: 'other-4', value: 'Other' }
  ];
  // drop-down list options
  amounts = Array(31).fill(0, 1, 31).map((x, i) => i);

  displayedColumns: string[] = ['item', 'amount', 'price', 'subtotal', 'delete'];
  // displayedTotalWithoutTaxesColumns: string[] = ['empty', 'netto', 'empty', 'nettototal', 'empty'];
  displayedTaxesColumns: string[] = ['empty', 'taxes', 'empty', 'taxtotal', 'empty'];
  displayedTotalColumns: string[] = ['empty', 'total', 'empty', 'pricetotal', 'empty'];
  itemHint = new FormControl('', [Validators.required]);

  ngOnInit() {
    this.fetchItems();
  }

  fetchItems() {
    this.api.getItems().subscribe(data => {
      this.shoppingCart = data;
      this.initArrays();
      this.shoppingCart.forEach(item => {
        this.taxes.push(item.amount * this.calculationService.calculateTax(item));
        this.nettos.push(item.amount * this.calculationService.calculateNetto(item));
        this.subtotals.push(item.amount * this.calculationService.calculatePrice(item));
        this.netto = this.calculateTotalPrice.transform(this.nettos);
        this.tax = this.calculateTotalPrice.transform(this.taxes);
        this.total = this.calculateTotalPrice.transform(this.subtotals);
      });
      // clear arrays
      this.initArrays();
    });
  }

  onSubmit(form: NgForm) {
    const value = form.value;
    const parsedString = this.calculationService.parseItemString(value.item);
    if (parsedString.item !== '' && parsedString.price !== '' && !isNaN(parseFloat(parsedString.price))) {
      value.item = parsedString.item;
      value.price = parsedString.price;
      if (parsedString.amount !== '') {
        value.amount = parsedString.amount;
      }
      this.api.addItem(value).subscribe(data => {
        this.fetchItems();
      });
      form.reset();
      form.controls.amount.setValue(1);
    } else {
      form.controls.item.setErrors({invalid: ''});
    }
  }

  onChangeAmount(item: ShoppingCart, amount: number) {
    item.amount = amount;
    this.api.updateItem(item.id, item).subscribe(data => {
      this.fetchItems();
    });
  }

  calculatePrice(item: ShoppingCart): number {
    return this.calculationService.calculatePrice(item);
  }

  onRemove(id: number) {
    this.api.deleteItem(id).subscribe(data => {
      this.fetchItems();
      this.clearVariables();
    });
  }

  onTruncateTable() {
    this.shoppingCart.forEach(item => {
      this.api.deleteItem(item.id).subscribe(data => {
        this.fetchItems();
        this.clearVariables();
      });
    });
  }

  getErrorMessage() {
    if (this.itemHint.hasError('required')) {
      return 'Item cannot be an empty string and it should be in format: [amount][imported] item at price';
    }
  }

  clearVariables() {
    this.total = 0;
    this.tax = 0;
  }

  initArrays() {
    this.subtotals = [];
    this.nettos = [];
    this.taxes = [];
  }

}
