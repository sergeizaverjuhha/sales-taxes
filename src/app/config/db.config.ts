import { Pool } from 'pg';

const connectionString = 'postgresql://postgres:postgres@192.168.99.100:5432/sales_taxes';
const dropTable = 'DROP TABLE IF EXISTS shoppingcart;'
const createTableString = `CREATE TABLE IF NOT EXISTS shoppingcart(
                            id SERIAL PRIMARY KEY,
                            item VARCHAR(100) NOT NULL,
                            category VARCHAR(50) NOT NULL,
                            amount INTEGER NOT NULL,
                            price NUMERIC NOT NULL);`

export const postgresdb = new Pool({
  connectionString: connectionString
});

// the pool will emit an error on behalf of any idle clients
// it contains if a backend error or network partition happens
postgresdb.on('error', (err, client) => {
  console.error('Unexpected error on idle client', err)
  process.exit(-1)
})

postgresdb.connect()
  .then(() => {
    console.log('Connection to PostgreSQL has been established successfully.');
    postgresdb.query(dropTable, (error, response) => {
      console.log('Droped table shoppingcart');
      postgresdb.query(createTableString, (error, response) => {
        console.log('Created table shoppingcart');
      });
    });
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });
